from lxml import etree
import xsd

ENVELOPE_NAMESPACE = "http://schemas.xmlsoap.org/soap/envelope/"
BINDING_NAMESPACE = "http://schemas.xmlsoap.org/wsdl/soap/"
CONTENT_TYPE = "text/xml"
class Code:
    CLIENT = "Client"
    SERVER = "Server"


def determin_soap_action(request):
    if request.META.get("HTTP_SOAPACTION"):
        return request.META.get("HTTP_SOAPACTION").replace('"','')
    elif request.META.get("HTTP_ACTION"):
        return request.META.get("HTTP_ACTION").replace('"','')
    else:
        return None
    
#TODO: merge with get_fault_response
def get_error_response(code,message):
    fault = Fault(faultcode="Client", faultstring=message)
    envelope = Envelope()
    envelope.Body = Body(Fault=fault)
    return envelope.xml("Envelope",namespace=ENVELOPE_NAMESPACE,
                        elementFormDefault=xsd.ElementFormDefault.QUALIFIED)
    
    

def get_fault_response(fault_exception):
    nsmap = {"soap":"http://schemas.xmlsoap.org/soap/envelope/"}
    
    #Build typical Fault section faultcode and faultstring
    fault = etree.Element("{http://schemas.xmlsoap.org/soap/envelope/}Fault")
    faultcode = etree.SubElement(fault,"faultcode")
    faultcode.text = "Client"
    faultstring = etree.SubElement(fault,"faultstring")
    faultstring.text = "Custom fault message, see detail section."
    
    #Build custom part, user defined data go under detail tag.
    if fault_exception.element_name:
        tagname = fault_exception.element_name
        element = fault_exception.fault.SCHEMA.get_element_by_name(tagname)
        namespace = element.namespace
    else:
        tagname = fault_exception.fault.__class__.__name__
        namespace = fault_exception.fault.SCHEMA.targetNamespace
    
    elementFormDefault = fault_exception.fault.SCHEMA.elementFormDefault     
    detail = etree.SubElement(fault,"detail")
    detail.append(fault_exception.fault.xmlelement(tagname,namespace,elementFormDefault))
    
    body = etree.Element("{http://schemas.xmlsoap.org/soap/envelope/}Body")
    body.append(fault)
    
    envelope = etree.Element("{http://schemas.xmlsoap.org/soap/envelope/}Envelope",nsmap=nsmap)
    envelope.append(body)
    
    return etree.tostring(envelope,pretty_print=True)



def parse_fault_message(fault):
    return fault.faultcode,fault.faultstring
    
def build_header(soapAction):
    return {"content-type" : CONTENT_TYPE,
            "SOAPAction"   : soapAction}
    
class Header(xsd.ComplexType):
    """SOAP Envelope Header."""
    pass

class Fault(xsd.ComplexType):
    """SOAP Envelope Fault."""
    faultcode = xsd.Element(xsd.String,namespace="")
    faultstring = xsd.Element(xsd.String,namespace="")
        
class Body(xsd.ComplexType):
    """SOAP Envelope Body."""
    message = xsd.ClassNamedElement(xsd.NamedType, minOccurs=0)
    Fault = xsd.Element(Fault, minOccurs=0)
    
    def content(self):
        return etree.tostring(self._xmlelement[0], pretty_print=True)
    
    def messageRoot(self):
        return self._xmlelement[0].tag.rsplit('}', 1)[-1]
    


class Envelope(xsd.ComplexType):
    """SOAP Envelope."""
    Header = xsd.Element(Header, nillable=True) 
    Body = xsd.Element(Body)

    @classmethod
    def reponse(cls, namespace,tagname,return_object):
        envelope = Envelope()
        envelope.Body = Body()
        envelope.Body.message = xsd.NamedType(name=tagname,value=return_object,namespace=namespace) 
        return envelope.xml("Envelope",namespace=ENVELOPE_NAMESPACE,
                elementFormDefault=xsd.ElementFormDefault.QUALIFIED)

SCHEMA = xsd.Schema(
    targetNamespace = ENVELOPE_NAMESPACE,
    elementFormDefault = xsd.ElementFormDefault.QUALIFIED,
    complexTypes = [Header, Body, Envelope, Fault])