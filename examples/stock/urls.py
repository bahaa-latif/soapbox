from django.conf.urls.defaults import patterns, include, url

# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()

urlpatterns = patterns('',    
     url(r'^stock/soap11$', "stock.web.views.dispatch11"),
     url(r'^stock/soap12$', "stock.web.views.dispatch12"),
     url(r'^ws/ops$', "stock.web.views.ops_dispatch"),
     url(r"^services/nptypes.xsd$", "stock.web.sp2server.generate_xsd"),
     url(r"^services/NpcdbService$", "stock.web.sp2server.dispatch"),
     url(r"^services2/NpcdbService$", "stock.web.sp22server.dispatch"),
     
)
