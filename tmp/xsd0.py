from soapbox import xsd
from soapbox.xsd import UNBOUNDED
from xsd2 import Schema as SCHEMA1

class AttachmentStorageType(xsd.String):    
    enumeration = [ "Linked",  "Embedded", ]
        
    
class BrokenRuleSeverity(xsd.String):    
    enumeration = [ "Error",  "Warning", ]
        
    
class DataFieldType(xsd.String):    
    enumeration = [ "Unknown",  "String",  "Numeric",  "Date",  "Boolean",  "FixedList",  "ListReference", ]
        
    
class DataFieldValidatorType(xsd.String):    
    enumeration = [ "MinValue",  "MaxValue",  "PositiveValue",  "NegativeValue",  "IntegerValue",  "Mandatory",  "StringMaxLength",  "StringMinLength", ]
        
    
class FindingType(xsd.String):    
    enumeration = [ "NonConformance",  "Observation", ]
        
    
class ServiceFaultCode(xsd.String):    
    enumeration = [ "SessionInvalid",  "AuthenticationFailed",  "ConnectionsNotAvailable",  "ReferenceListTypeNotFound",  "ManagedListTypeNotFound",  "OperationNotSupported",  "InvalidDatabase",  "ItemNotFoundInDatabase",  "UnableToDeleteItem",  "CannotConnectToService",  "InvalidOperation",  "UserLockedOut",  "NullParameterReceived",  "UnhandledException",  "XmlDeserializationException",  "UnsufficientPermission",  "QPulseWebLocationMissing", ]
        
    




class Incident(xsd.ComplexType):
    INHERITANCE = None
    INDICATOR = xsd.Sequence
    Attachments = xsd.Element("ArrayOfAttachment", minOccurs=0,nillable=True)
    Classifications = xsd.Element("ArrayOfClassification", minOccurs=0,nillable=True)
    ClosedByPerson = xsd.Element(xsd.String, minOccurs=0,nillable=True)
    ClosedDate = xsd.Element(xsd.DateTime, minOccurs=0)
    Costs = xsd.Element("ArrayOfIncidentCost", minOccurs=0,nillable=True)
    Currency = xsd.Element(xsd.String, minOccurs=0,nillable=True)
    DateCreated = xsd.Element(xsd.DateTime, minOccurs=0)
    Department = xsd.Element(xsd.String, minOccurs=0,nillable=True)
    EventDate = xsd.Element(xsd.DateTime, minOccurs=0)
    HasMainOccurrence = xsd.Element(xsd.Boolean, minOccurs=0)
    ID = xsd.Element(xsd.Int, minOccurs=0)
    IsMandatoryReport = xsd.Element(xsd.Boolean, minOccurs=0)
    IsOverdue = xsd.Element(xsd.Boolean, minOccurs=0)
    Notes = xsd.Element("ArrayOfNote", minOccurs=0,nillable=True)
    Number = xsd.Element(xsd.String, minOccurs=0,nillable=True)
    OccurrenceIDs = xsd.Element("ArrayOfint", minOccurs=0,nillable=True)
    Occurrences = xsd.Element("ArrayOfOccurrence", minOccurs=0,nillable=True)
    Owner = xsd.Element(xsd.String, minOccurs=0,nillable=True)
    OwnerId = xsd.Element(xsd.Int, minOccurs=0)
    Priority = xsd.Element(xsd.String, minOccurs=0,nillable=True)
    RegulatorNumber = xsd.Element(xsd.String, minOccurs=0,nillable=True)
    RelatedDocument = xsd.Element("DocumentRevisionStatus", minOccurs=0,nillable=True)
    ReportSent = xsd.Element(xsd.DateTime, minOccurs=0)
    ReportTypeName = xsd.Element(xsd.String, minOccurs=0,nillable=True)
    Stages = xsd.Element("ArrayOfIncidentStage", minOccurs=0,nillable=True)
    Status = xsd.Element(xsd.String, minOccurs=0,nillable=True)
    TargetDate = xsd.Element(xsd.DateTime, minOccurs=0)
    Title = xsd.Element(xsd.String, minOccurs=0,nillable=True)
    TotalCost = xsd.Element(xsd.Decimal, minOccurs=0)
    TotalTime = xsd.Element(xsd.Int, minOccurs=0)

    @classmethod
    def create(cls, ):
        instance = cls()
        return instance


class ArrayOfAttachment(xsd.ComplexType):
    INHERITANCE = None
    INDICATOR = xsd.Sequence
    Attachments = xsd.ListElement("Attachment","Attachment", minOccurs=0, maxOccurs=UNBOUNDED,nillable=True)

    @classmethod
    def create(cls, ):
        instance = cls()
        return instance


class Attachment(xsd.ComplexType):
    INHERITANCE = None
    INDICATOR = xsd.Sequence
    ID = xsd.Element(xsd.Int, minOccurs=0)
    Items = xsd.Element("ArrayOfAttachmentItem", minOccurs=0,nillable=True)
    Name = xsd.Element(xsd.String, minOccurs=0,nillable=True)
    StorageType = xsd.Element("AttachmentStorageType", minOccurs=0)

    @classmethod
    def create(cls, ):
        instance = cls()
        return instance


class ArrayOfAttachmentItem(xsd.ComplexType):
    INHERITANCE = None
    INDICATOR = xsd.Sequence
    AttachmentItems = xsd.ListElement("AttachmentItem","AttachmentItem", minOccurs=0, maxOccurs=UNBOUNDED,nillable=True)

    @classmethod
    def create(cls, ):
        instance = cls()
        return instance


class AttachmentItem(xsd.ComplexType):
    INHERITANCE = None
    INDICATOR = xsd.Sequence
    AbsolutePath = xsd.Element(xsd.String, minOccurs=0,nillable=True)
    DisplayFileName = xsd.Element(xsd.String, minOccurs=0,nillable=True)
    FileSize = xsd.Element(xsd.Long, minOccurs=0)
    IsIndex = xsd.Element(xsd.Boolean, minOccurs=0)
    ModifiedDate = xsd.Element(xsd.DateTime, minOccurs=0)
    RelativePath = xsd.Element(xsd.String, minOccurs=0,nillable=True)

    @classmethod
    def create(cls, ):
        instance = cls()
        return instance


class ArrayOfClassification(xsd.ComplexType):
    INHERITANCE = None
    INDICATOR = xsd.Sequence
    Classifications = xsd.ListElement("Classification","Classification", minOccurs=0, maxOccurs=UNBOUNDED,nillable=True)

    @classmethod
    def create(cls, ):
        instance = cls()
        return instance


class Classification(xsd.ComplexType):
    INHERITANCE = None
    INDICATOR = xsd.Sequence
    ClassificationPath = xsd.Element(xsd.String, minOccurs=0,nillable=True)
    DisplayValue = xsd.Element(xsd.String, minOccurs=0,nillable=True)
    ListName = xsd.Element(xsd.String, minOccurs=0,nillable=True)

    @classmethod
    def create(cls, ):
        instance = cls()
        return instance


class ArrayOfIncidentCost(xsd.ComplexType):
    INHERITANCE = None
    INDICATOR = xsd.Sequence
    IncidentCosts = xsd.ListElement("IncidentCost","IncidentCost", minOccurs=0, maxOccurs=UNBOUNDED,nillable=True)

    @classmethod
    def create(cls, ):
        instance = cls()
        return instance


class IncidentCost(xsd.ComplexType):
    INHERITANCE = None
    INDICATOR = xsd.Sequence
    Cost = xsd.Element(xsd.Decimal, minOccurs=0)
    CostCategoryName = xsd.Element(xsd.String, minOccurs=0,nillable=True)

    @classmethod
    def create(cls, ):
        instance = cls()
        return instance


class ArrayOfNote(xsd.ComplexType):
    INHERITANCE = None
    INDICATOR = xsd.Sequence
    Notes = xsd.ListElement("Note","Note", minOccurs=0, maxOccurs=UNBOUNDED,nillable=True)

    @classmethod
    def create(cls, ):
        instance = cls()
        return instance


class Note(xsd.ComplexType):
    INHERITANCE = None
    INDICATOR = xsd.Sequence
    CreatedByPersonName = xsd.Element(xsd.String, minOccurs=0,nillable=True)
    DateCreated = xsd.Element(xsd.DateTime, minOccurs=0,nillable=True)
    ID = xsd.Element(xsd.Int, minOccurs=0)
    NoteSubject = xsd.Element(xsd.String, minOccurs=0,nillable=True)
    NoteText = xsd.Element(xsd.String, minOccurs=0,nillable=True)
    ParentID = xsd.Element(xsd.Int, minOccurs=0)

    @classmethod
    def create(cls, ):
        instance = cls()
        return instance


class ArrayOfOccurrence(xsd.ComplexType):
    INHERITANCE = None
    INDICATOR = xsd.Sequence
    Occurrences = xsd.ListElement("Occurrence","Occurrence", minOccurs=0, maxOccurs=UNBOUNDED,nillable=True)

    @classmethod
    def create(cls, ):
        instance = cls()
        return instance


class Occurrence(xsd.ComplexType):
    INHERITANCE = None
    INDICATOR = xsd.Sequence
    Attachments = xsd.Element("ArrayOfAttachment", minOccurs=0,nillable=True)
    BrokenRules = xsd.Element("ArrayOfBrokenRule", minOccurs=0,nillable=True)
    DataFields = xsd.Element("ArrayOfDataField", minOccurs=0,nillable=True)
    ID = xsd.Element(xsd.Int, minOccurs=0)
    IncidentId = xsd.Element(xsd.Int, minOccurs=0)
    IncidentNumber = xsd.Element(xsd.String, minOccurs=0,nillable=True)
    IncidentReportSentDate = xsd.Element(xsd.DateTime, minOccurs=0,nillable=True)
    IncidentStatusName = xsd.Element(xsd.String, minOccurs=0,nillable=True)
    IncidentTitle = xsd.Element(xsd.String, minOccurs=0,nillable=True)
    IsConfidential = xsd.Element(xsd.Boolean, minOccurs=0)
    IsMOR = xsd.Element(xsd.Boolean, minOccurs=0)
    IsMain = xsd.Element(xsd.Boolean, minOccurs=0)
    IsRejected = xsd.Element(xsd.Boolean, minOccurs=0)
    Notes = xsd.Element("ArrayOfNote", minOccurs=0,nillable=True)
    Number = xsd.Element(xsd.String, minOccurs=0,nillable=True)
    RecordedByPersonName = xsd.Element(xsd.String, minOccurs=0,nillable=True)
    RecordedDate = xsd.Element(xsd.DateTime, minOccurs=0,nillable=True)
    ReportTypeName = xsd.Element(xsd.String, minOccurs=0,nillable=True)
    ReportedByPersonName = xsd.Element(xsd.String, minOccurs=0,nillable=True)
    ReportedDate = xsd.Element(xsd.DateTime, minOccurs=0,nillable=True)
    Title = xsd.Element(xsd.String, minOccurs=0,nillable=True)

    @classmethod
    def create(cls, ):
        instance = cls()
        return instance


class ArrayOfBrokenRule(xsd.ComplexType):
    INHERITANCE = None
    INDICATOR = xsd.Sequence
    BrokenRules = xsd.ListElement("BrokenRule","BrokenRule", minOccurs=0, maxOccurs=UNBOUNDED,nillable=True)

    @classmethod
    def create(cls, ):
        instance = cls()
        return instance


class BrokenRule(xsd.ComplexType):
    INHERITANCE = None
    INDICATOR = xsd.Sequence
    Description = xsd.Element(xsd.String, minOccurs=0,nillable=True)
    PropertyName = xsd.Element(xsd.String, minOccurs=0,nillable=True)
    Severity = xsd.Element("BrokenRuleSeverity", minOccurs=0)

    @classmethod
    def create(cls, ):
        instance = cls()
        return instance


class ArrayOfDataField(xsd.ComplexType):
    INHERITANCE = None
    INDICATOR = xsd.Sequence
    DataFields = xsd.ListElement("DataField","DataField", minOccurs=0, maxOccurs=UNBOUNDED,nillable=True)

    @classmethod
    def create(cls, ):
        instance = cls()
        return instance


class DataField(xsd.ComplexType):
    INHERITANCE = None
    INDICATOR = xsd.Sequence
    DataType = xsd.Element("DataFieldType", minOccurs=0)
    Description = xsd.Element(xsd.String, minOccurs=0,nillable=True)
    DisplayLabel = xsd.Element(xsd.String, minOccurs=0,nillable=True)
    ElementIdentifier = xsd.Element(xsd.String, minOccurs=0,nillable=True)
    FixedListItems = xsd.Element("ArrayOfFixedListItem", minOccurs=0,nillable=True)
    ListItemDisplayValue = xsd.Element(xsd.String, minOccurs=0,nillable=True)
    MultipleValuesAllowed = xsd.Element(xsd.Boolean, minOccurs=0)
    Name = xsd.Element(xsd.String, minOccurs=0,nillable=True)
    ReferenceListName = xsd.Element(xsd.String, minOccurs=0,nillable=True)
    Validators = xsd.Element("ArrayOfDataFieldValidator", minOccurs=0,nillable=True)
    Value = xsd.Element(xsd.String, minOccurs=0,nillable=True)

    @classmethod
    def create(cls, ):
        instance = cls()
        return instance


class ArrayOfFixedListItem(xsd.ComplexType):
    INHERITANCE = None
    INDICATOR = xsd.Sequence
    FixedListItems = xsd.ListElement("FixedListItem","FixedListItem", minOccurs=0, maxOccurs=UNBOUNDED,nillable=True)

    @classmethod
    def create(cls, ):
        instance = cls()
        return instance


class FixedListItem(xsd.ComplexType):
    INHERITANCE = None
    INDICATOR = xsd.Sequence
    Value = xsd.Element(xsd.String, minOccurs=0,nillable=True)

    @classmethod
    def create(cls, ):
        instance = cls()
        return instance


class ArrayOfDataFieldValidator(xsd.ComplexType):
    INHERITANCE = None
    INDICATOR = xsd.Sequence
    DataFieldValidators = xsd.ListElement("DataFieldValidator","DataFieldValidator", minOccurs=0, maxOccurs=UNBOUNDED,nillable=True)

    @classmethod
    def create(cls, ):
        instance = cls()
        return instance


class DataFieldValidator(xsd.ComplexType):
    INHERITANCE = None
    INDICATOR = xsd.Sequence
    Parameter = xsd.Element(xsd.String, minOccurs=0,nillable=True)
    ValidatorType = xsd.Element("DataFieldValidatorType", minOccurs=0)

    @classmethod
    def create(cls, ):
        instance = cls()
        return instance


class DocumentRevisionStatus(xsd.ComplexType):
    INHERITANCE = None
    INDICATOR = xsd.Sequence
    ActiveRevCanViewFile = xsd.Element(xsd.Boolean, minOccurs=0)
    ActiveRevName = xsd.Element(xsd.String, minOccurs=0,nillable=True)
    ActiveRevPath = xsd.Element(xsd.String, minOccurs=0,nillable=True)
    ActiveRevReference = xsd.Element(xsd.String, minOccurs=0,nillable=True)
    ActiveRevTitle = xsd.Element(xsd.String, minOccurs=0,nillable=True)
    ActiveRevisionID = xsd.Element(xsd.Int, minOccurs=0)
    CanViewFile = xsd.Element(xsd.Boolean, minOccurs=0)
    CurrentRevisionReference = xsd.Element(xsd.String, minOccurs=0,nillable=True)
    ID = xsd.Element(xsd.Int, minOccurs=0)

    @classmethod
    def create(cls, ):
        instance = cls()
        return instance


class ArrayOfIncidentStage(xsd.ComplexType):
    INHERITANCE = None
    INDICATOR = xsd.Sequence
    IncidentStages = xsd.ListElement("IncidentStage","IncidentStage", minOccurs=0, maxOccurs=UNBOUNDED,nillable=True)

    @classmethod
    def create(cls, ):
        instance = cls()
        return instance


class IncidentStage(xsd.ComplexType):
    INHERITANCE = None
    INDICATOR = xsd.Sequence
    Actions = xsd.Element("ArrayOfIncidentStageAction", minOccurs=0,nillable=True)
    Attachments = xsd.Element("ArrayOfAttachment", minOccurs=0,nillable=True)
    ClosedByPersonName = xsd.Element(xsd.String, minOccurs=0,nillable=True)
    Description = xsd.Element(xsd.String, minOccurs=0,nillable=True)
    Findings = xsd.Element("ArrayOfIncidentStageFinding", minOccurs=0,nillable=True)
    OwnerPersonName = xsd.Element(xsd.String, minOccurs=0,nillable=True)
    StageTypeName = xsd.Element(xsd.String, minOccurs=0,nillable=True)
    TargetDate = xsd.Element(xsd.DateTime, minOccurs=0)
    TeamMembers = xsd.Element("ArrayOfTeamMember", minOccurs=0,nillable=True)

    @classmethod
    def create(cls, ):
        instance = cls()
        return instance


class ArrayOfIncidentStageAction(xsd.ComplexType):
    INHERITANCE = None
    INDICATOR = xsd.Sequence
    IncidentStageActions = xsd.ListElement("IncidentStageAction","IncidentStageAction", minOccurs=0, maxOccurs=UNBOUNDED,nillable=True)

    @classmethod
    def create(cls, ):
        instance = cls()
        return instance


class IncidentStageAction(xsd.ComplexType):
    INHERITANCE = None
    INDICATOR = xsd.Sequence
    ClosedDateTime = xsd.Element(xsd.DateTime, minOccurs=0)
    Description = xsd.Element(xsd.String, minOccurs=0,nillable=True)
    IsClosed = xsd.Element(xsd.Boolean, minOccurs=0)
    Number = xsd.Element(xsd.String, minOccurs=0,nillable=True)
    OwnerPersonName = xsd.Element(xsd.String, minOccurs=0,nillable=True)
    ShortNumber = xsd.Element(xsd.String, minOccurs=0,nillable=True)

    @classmethod
    def create(cls, ):
        instance = cls()
        return instance


class ArrayOfIncidentStageFinding(xsd.ComplexType):
    INHERITANCE = None
    INDICATOR = xsd.Sequence
    IncidentStageFindings = xsd.ListElement("IncidentStageFinding","IncidentStageFinding", minOccurs=0, maxOccurs=UNBOUNDED,nillable=True)

    @classmethod
    def create(cls, ):
        instance = cls()
        return instance


class IncidentStageFinding(xsd.ComplexType):
    INHERITANCE = None
    INDICATOR = xsd.Sequence
    Classifications = xsd.Element("ArrayOfClassification", minOccurs=0,nillable=True)
    Description = xsd.Element(xsd.String, minOccurs=0,nillable=True)
    DisplayValue = xsd.Element(xsd.String, minOccurs=0,nillable=True)
    FindingType = xsd.Element("FindingType", minOccurs=0)
    Number = xsd.Element(xsd.String, minOccurs=0,nillable=True)
    RelatedNonConformanceID = xsd.Element(xsd.Int, minOccurs=0)
    RiskAssessments = xsd.Element("ArrayOfRiskAssessment", minOccurs=0,nillable=True)
    Status = xsd.Element(xsd.String, minOccurs=0,nillable=True)

    @classmethod
    def create(cls, ):
        instance = cls()
        return instance


class ArrayOfRiskAssessment(xsd.ComplexType):
    INHERITANCE = None
    INDICATOR = xsd.Sequence
    RiskAssessments = xsd.ListElement("RiskAssessment","RiskAssessment", minOccurs=0, maxOccurs=UNBOUNDED,nillable=True)

    @classmethod
    def create(cls, ):
        instance = cls()
        return instance


class RiskAssessment(xsd.ComplexType):
    INHERITANCE = None
    INDICATOR = xsd.Sequence
    Comment = xsd.Element(xsd.String, minOccurs=0,nillable=True)
    MatrixName = xsd.Element(xsd.String, minOccurs=0,nillable=True)
    MatrixXAxisTitle = xsd.Element(xsd.String, minOccurs=0,nillable=True)
    MatrixXValue = xsd.Element(xsd.String, minOccurs=0,nillable=True)
    MatrixYAxisTitle = xsd.Element(xsd.String, minOccurs=0,nillable=True)
    MatrixYValue = xsd.Element(xsd.String, minOccurs=0,nillable=True)
    RiskValue = xsd.Element(xsd.String, minOccurs=0,nillable=True)
    SuggestedAction = xsd.Element(xsd.String, minOccurs=0,nillable=True)
    Title = xsd.Element(xsd.String, minOccurs=0,nillable=True)

    @classmethod
    def create(cls, ):
        instance = cls()
        return instance


class ArrayOfTeamMember(xsd.ComplexType):
    INHERITANCE = None
    INDICATOR = xsd.Sequence
    TeamMembers = xsd.ListElement("TeamMember","TeamMember", minOccurs=0, maxOccurs=UNBOUNDED,nillable=True)

    @classmethod
    def create(cls, ):
        instance = cls()
        return instance


class TeamMember(xsd.ComplexType):
    INHERITANCE = None
    INDICATOR = xsd.Sequence
    DepartmentOrganisation = xsd.Element(xsd.String, minOccurs=0,nillable=True)
    PersonID = xsd.Element(xsd.Int, minOccurs=0)
    PersonName = xsd.Element(xsd.String, minOccurs=0,nillable=True)

    @classmethod
    def create(cls, ):
        instance = cls()
        return instance


class ServiceFault(xsd.ComplexType):
    INHERITANCE = None
    INDICATOR = xsd.Sequence
    FaultCode = xsd.Element("ServiceFaultCode", minOccurs=0)
    Message = xsd.Element(xsd.String, minOccurs=0,nillable=True)

    @classmethod
    def create(cls, ):
        instance = cls()
        return instance


class IncidentQuery(xsd.ComplexType):
    INHERITANCE = None
    INDICATOR = xsd.Sequence
    EventDateOnOrAfter = xsd.Element(xsd.DateTime, minOccurs=0,nillable=True)
    EventDateOnOrBefore = xsd.Element(xsd.DateTime, minOccurs=0,nillable=True)
    IsMandatory = xsd.Element(xsd.Boolean, minOccurs=0,nillable=True)
    Keywords = xsd.Element(xsd.String, minOccurs=0,nillable=True)
    Number = xsd.Element(xsd.String, minOccurs=0,nillable=True)
    OwnerName = xsd.Element(xsd.String, minOccurs=0,nillable=True)
    PriorityName = xsd.Element(xsd.String, minOccurs=0,nillable=True)
    ReportTypeName = xsd.Element(xsd.String, minOccurs=0,nillable=True)
    StatusName = xsd.Element(xsd.String, minOccurs=0,nillable=True)

    @classmethod
    def create(cls, ):
        instance = cls()
        return instance


class ArrayOfIncident(xsd.ComplexType):
    INHERITANCE = None
    INDICATOR = xsd.Sequence
    Incidents = xsd.ListElement("Incident","Incident", minOccurs=0, maxOccurs=UNBOUNDED,nillable=True)

    @classmethod
    def create(cls, ):
        instance = cls()
        return instance





class GetIncident(xsd.ComplexType):
    INHERITANCE = None
    INDICATOR = xsd.Sequence
    token = xsd.Element(xsd.String, minOccurs=0,nillable=True)
    incidentID = xsd.Element(xsd.Int, minOccurs=0)
    includeOccurrences = xsd.Element(xsd.Boolean, minOccurs=0)

    @classmethod
    def create(cls, ):
        instance = cls()
        return instance
    



class GetIncidentResponse(xsd.ComplexType):
    INHERITANCE = None
    INDICATOR = xsd.Sequence
    GetIncidentResult = xsd.Element("Incident", minOccurs=0,nillable=True)

    @classmethod
    def create(cls, ):
        instance = cls()
        return instance
    



class GetIncidents(xsd.ComplexType):
    INHERITANCE = None
    INDICATOR = xsd.Sequence
    token = xsd.Element(xsd.String, minOccurs=0,nillable=True)
    query = xsd.Element("IncidentQuery", minOccurs=0,nillable=True)
    includeOccurrences = xsd.Element(xsd.Boolean, minOccurs=0)

    @classmethod
    def create(cls, ):
        instance = cls()
        return instance
    



class GetIncidentsResponse(xsd.ComplexType):
    INHERITANCE = None
    INDICATOR = xsd.Sequence
    GetIncidentsResult = xsd.Element("ArrayOfIncident", minOccurs=0,nillable=True)

    @classmethod
    def create(cls, ):
        instance = cls()
        return instance
    



class GetIncidentIDs(xsd.ComplexType):
    INHERITANCE = None
    INDICATOR = xsd.Sequence
    token = xsd.Element(xsd.String, minOccurs=0,nillable=True)
    query = xsd.Element("IncidentQuery", minOccurs=0,nillable=True)

    @classmethod
    def create(cls, ):
        instance = cls()
        return instance
    



class GetIncidentIDsResponse(xsd.ComplexType):
    INHERITANCE = None
    INDICATOR = xsd.Sequence
    GetIncidentIDsResult = xsd.Element("ArrayOfint", minOccurs=0,nillable=True)

    @classmethod
    def create(cls, ):
        instance = cls()
        return instance
    

Schema = xsd.Schema(
    location = "http://ec2-46-137-40-70.eu-west-1.compute.amazonaws.com/QPulse5WebServices/Services/Incident.svc?xsd=xsd0",
    imports = [SCHEMA1,],
    targetNamespace = "http://www.qpulse.com/QPulseWebServices/v1.1/",
    elementFormDefault = "qualified",
    simpleTypes = [ AttachmentStorageType, BrokenRuleSeverity, DataFieldType, DataFieldValidatorType, FindingType, ServiceFaultCode,],
    attributeGroups = [],
    groups = [],
    complexTypes = [ Incident, ArrayOfAttachment, Attachment, ArrayOfAttachmentItem, AttachmentItem, ArrayOfClassification, Classification, ArrayOfIncidentCost, IncidentCost, ArrayOfNote, Note, ArrayOfOccurrence, Occurrence, ArrayOfBrokenRule, BrokenRule, ArrayOfDataField, DataField, ArrayOfFixedListItem, FixedListItem, ArrayOfDataFieldValidator, DataFieldValidator, DocumentRevisionStatus, ArrayOfIncidentStage, IncidentStage, ArrayOfIncidentStageAction, IncidentStageAction, ArrayOfIncidentStageFinding, IncidentStageFinding, ArrayOfRiskAssessment, RiskAssessment, ArrayOfTeamMember, TeamMember, ServiceFault, IncidentQuery, ArrayOfIncident,],
    elements = {  "GetIncident":xsd.Element(GetIncident()), "GetIncidentResponse":xsd.Element(GetIncidentResponse()), "Incident":xsd.Element("Incident"), "ArrayOfAttachment":xsd.Element("ArrayOfAttachment"), "Attachment":xsd.Element("Attachment"), "ArrayOfAttachmentItem":xsd.Element("ArrayOfAttachmentItem"), "AttachmentItem":xsd.Element("AttachmentItem"), "AttachmentStorageType":xsd.Element("AttachmentStorageType"), "ArrayOfClassification":xsd.Element("ArrayOfClassification"), "Classification":xsd.Element("Classification"), "ArrayOfIncidentCost":xsd.Element("ArrayOfIncidentCost"), "IncidentCost":xsd.Element("IncidentCost"), "ArrayOfNote":xsd.Element("ArrayOfNote"), "Note":xsd.Element("Note"), "ArrayOfOccurrence":xsd.Element("ArrayOfOccurrence"), "Occurrence":xsd.Element("Occurrence"), "ArrayOfBrokenRule":xsd.Element("ArrayOfBrokenRule"), "BrokenRule":xsd.Element("BrokenRule"), "BrokenRuleSeverity":xsd.Element("BrokenRuleSeverity"), "ArrayOfDataField":xsd.Element("ArrayOfDataField"), "DataField":xsd.Element("DataField"), "DataFieldType":xsd.Element("DataFieldType"), "ArrayOfFixedListItem":xsd.Element("ArrayOfFixedListItem"), "FixedListItem":xsd.Element("FixedListItem"), "ArrayOfDataFieldValidator":xsd.Element("ArrayOfDataFieldValidator"), "DataFieldValidator":xsd.Element("DataFieldValidator"), "DataFieldValidatorType":xsd.Element("DataFieldValidatorType"), "DocumentRevisionStatus":xsd.Element("DocumentRevisionStatus"), "ArrayOfIncidentStage":xsd.Element("ArrayOfIncidentStage"), "IncidentStage":xsd.Element("IncidentStage"), "ArrayOfIncidentStageAction":xsd.Element("ArrayOfIncidentStageAction"), "IncidentStageAction":xsd.Element("IncidentStageAction"), "ArrayOfIncidentStageFinding":xsd.Element("ArrayOfIncidentStageFinding"), "IncidentStageFinding":xsd.Element("IncidentStageFinding"), "FindingType":xsd.Element("FindingType"), "ArrayOfRiskAssessment":xsd.Element("ArrayOfRiskAssessment"), "RiskAssessment":xsd.Element("RiskAssessment"), "ArrayOfTeamMember":xsd.Element("ArrayOfTeamMember"), "TeamMember":xsd.Element("TeamMember"), "ServiceFault":xsd.Element("ServiceFault"), "ServiceFaultCode":xsd.Element("ServiceFaultCode"), "GetIncidents":xsd.Element(GetIncidents()), "IncidentQuery":xsd.Element("IncidentQuery"), "GetIncidentsResponse":xsd.Element(GetIncidentsResponse()), "ArrayOfIncident":xsd.Element("ArrayOfIncident"), "GetIncidentIDs":xsd.Element(GetIncidentIDs()), "GetIncidentIDsResponse":xsd.Element(GetIncidentIDsResponse()),})
