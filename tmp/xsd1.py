from soapbox import xsd
from soapbox.xsd import UNBOUNDED

class Char(xsd.Int):
    pass
        
    
class Duration(xsd.Duration):
    pattern = r"\-?P(\d*D)?(T(\d*H)?(\d*M)?(\d*(\.\d*)?S)?)?"
    minInclusive = r"-P10675199DT2H48M5.4775808S"
    maxInclusive = r"P10675199DT2H48M5.4775807S"
    
class Guid(xsd.String):
    pattern = r"[\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}"
    





Schema = xsd.Schema(
    location = "http://ec2-46-137-40-70.eu-west-1.compute.amazonaws.com/QPulse5WebServices/Services/Incident.svc?xsd=xsd1",
    imports = [],
    targetNamespace = "http://schemas.microsoft.com/2003/10/Serialization/",
    elementFormDefault = "qualified",
    simpleTypes = [ Char, Duration, Guid,],
    attributeGroups = [],
    groups = [],
    complexTypes = [],
    elements = {  "anyType":xsd.Element(xsd.AnyType), "anyURI":xsd.Element(xsd.AnyURI), "base64Binary":xsd.Element(xsd.Base64Binary), "boolean":xsd.Element(xsd.Boolean), "byte":xsd.Element(xsd.Byte), "dateTime":xsd.Element(xsd.DateTime), "decimal":xsd.Element(xsd.Decimal), "double":xsd.Element(xsd.Double), "float":xsd.Element(xsd.Float), "int":xsd.Element(xsd.Int), "long":xsd.Element(xsd.Long), "QName":xsd.Element(xsd.QName), "short":xsd.Element(xsd.Short), "string":xsd.Element(xsd.String), "unsignedByte":xsd.Element(xsd.UnsignedByte), "unsignedInt":xsd.Element(xsd.UnsignedInt), "unsignedLong":xsd.Element(xsd.UnsignedLong), "unsignedShort":xsd.Element(xsd.UnsignedShort), "char":xsd.Element("Char"), "duration":xsd.Element("Duration"), "guid":xsd.Element("Guid"),})
