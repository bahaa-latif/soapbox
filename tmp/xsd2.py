from soapbox import xsd
from soapbox.xsd import UNBOUNDED



class ArrayOfint(xsd.ComplexType):
    INHERITANCE = None
    INDICATOR = xsd.Sequence
    ints = xsd.ListElement(xsd.Int,"int", minOccurs=0, maxOccurs=UNBOUNDED)

    @classmethod
    def create(cls, ):
        instance = cls()
        return instance



Schema = xsd.Schema(
    location = "http://ec2-46-137-40-70.eu-west-1.compute.amazonaws.com/QPulse5WebServices/Services/Incident.svc?xsd=xsd2",
    imports = [],
    targetNamespace = "http://schemas.microsoft.com/2003/10/Serialization/Arrays",
    elementFormDefault = "qualified",
    simpleTypes = [],
    attributeGroups = [],
    groups = [],
    complexTypes = [ ArrayOfint,],
    elements = {  "ArrayOfint":xsd.Element("ArrayOfint"),})
